section .text
 
SYS_EXIT equ 60
SYS_READ equ 0
SYS_WRITE equ 1
STD_IN equ 0
STD_OUT equ 1

; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, SYS_EXIT,
    syscall 

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax              

.loop:
    cmp byte [rdi + rax], 0              
    jz .end                   
    inc rax                   
    jmp .loop                

.end:
    ret



; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi            
    call string_length  
    pop rsi             
    mov rdx, rax       
    mov rax, SYS_WRITE
    mov rdi, STD_OUT     
    syscall             
    ret


; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, `\n`

; Принимает код символа и выводит его в stdout
print_char:
    push rdi             
    mov [rsp], dil       
    mov rax, SYS_WRITE
    mov rdi, STD_OUT     
    mov rsi, rsp         
    mov rdx, 1          
    syscall              
    pop rdi           
    ret


; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi
    mov rcx, 10
    sub rsp, 32      ; Выделяем место в стеке для временной строки
    lea rsi, [rsp + 31]
    mov byte [rsi], 0

.loop:
    dec rsi
    xor rdx, rdx
    div rcx
    add dl, '0'
    mov [rsi], dl
    test rax, rax
    jnz .loop

    mov rdi, rsi
    call print_string
    add rsp, 32
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test rdi, rdi
	jge .print_uint
	neg rdi
	push rdi
	mov rdi, '-'
	call print_char
	pop rdi
.print_uint:
	jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    push  rbx
    xor   rax, rax
.loop:
    mov   cl, [rdi+rax]
    mov   bl, [rsi+rax]
    cmp   bl, cl
    jne   .neg
    test  bl, bl
    je    .pos
    inc   rax
    jmp   .loop
.pos:
    mov   rax, 1
    pop   rbx
    ret
.neg:
    xor   rax, rax
    pop   rbx
    ret


; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    dec       rsp
    mov       rsi, rsp
    mov       rax, SYS_READ
    xor       rdi, rdi
    mov       rdx, 1
    syscall
    test      rax, rax
    jz        .end
    xor       rax, rax
    mov al,   [rsp]
.end:
    inc       rsp
    ret


; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    push r12             
    push r13              
    push r14              
    mov  r12, rdi        
    mov  r13, rsi        
    xor  r14, r14        
.trim:
    call read_char
    cmp  al, ' '
    je   .trim
    cmp  al, `\t`
    je   .trim
    cmp  al, `\n`
    je   .trim
.loop:
    test al, al
    jz   .end
    cmp  r14, r13         
    je  .drop
    cmp  al, ' '
    je   .end
    cmp  al, `\t`
    je   .end
    cmp  al, `\n`
    je   .end
    mov  [r12+r14], al   
    inc  r14
    call read_char
    jmp  .loop
.drop:
    xor  rax, rax
    pop  r12
    pop  r13
    pop  r14
    ret
.end:
    cmp  r14, r13
    je   .drop
    mov  rax, r12
    mov  byte [r12+r14], 0
    mov  rdx, r14
    pop  r12
    pop  r13
    pop  r14
    ret

 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rcx, rcx
    xor r8,  r8
    mov r9,  10
.loop:
    mov cl,  [rdi+r8]
    cmp cl,  '0'
    jl  .end
    cmp cl,  '9'
    jg  .end
    sub cl,  '0'
    mul r9
    add rax, rcx
    inc r8
    jmp .loop
.end:
    mov rdx, r8
    ret


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rsi, rsi
	cmp byte[rdi], '-'
	je .neg
	cmp byte[rdi], '+'
	je .pos
	jmp parse_uint
	
.neg:
	inc rdi
	call parse_uint
	inc rdx
	neg rax
	ret
	
.pos:
	inc rdi
	call parse_uint
	inc rdx
	ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
.loop:
    cmp rax, rdx              
    jnb .drop
    mov cl, [rdi + rax]       
    mov byte[rsi + rax], cl   
    cmp cl, 0
    jz .end
    inc rax
    jmp .loop
.drop:
    xor rax, rax
    ret
.end:
    inc rax
    ret

